import { Alert,} from 'react-native';
import * as services from "../Service/services"
import * as Url from "../Service/Url"
import * as Utility from "../utility/index"

export let LoginApi=async(email,password)=>{
    console.log("email",email)
    console.log("email",password)
    if(await Utility.isFieldEmpty(email,password)){
        return Alert.alert("Please fill all fields")
    }
    else if(await Utility.isValidEmail(email)){
        return Alert.alert("Please fill valid email")
    }
    else{
    let body={
        email:email,
        password:password
    }
    console.log("body",body)
    let res=await services.post(Url.LOGIN,"",body)
    // console.log("response login",res.data.apiToken)
    if(res.isSuccess==true){
        console.log("sucess true")

        await Utility.setInLocalStorge("token",res.data.apiToken)
    }
    
return res;
    }
}

export let SignUpApi=async(FirstName,LastName,email,password)=>{
    console.log("FirstName",FirstName)
    console.log("LastName",LastName)
    console.log("email",email)
    console.log("email",password)
    if(await Utility.isFieldEmpty(FirstName,LastName,email,password)){
        return Alert.alert("Please fill all fields")
    }
    else if(await Utility.isValidEmail(email)){
        return Alert.alert("Please fill valid email")
    }
    else{
    let body={
        firstName:FirstName,
        lastName:LastName,
        email:email,
        password:password,
        role:"USER"
    }
    console.log("body",body)
    let res=await services.post(Url.SIGN_UP,"",body)
    console.log("response login",res.isSuccess)
   
return res;
    }
}
export let ForgotPasswordApi=async(email)=>{
 
    console.log("email",email)
    if(await Utility.isFieldEmpty(email)){
        return Alert.alert("Please fill all fields")
    }
    else if(await Utility.isValidEmail(email)){
        return Alert.alert("Please fill valid email")
    }
    else{
    let body={
     
        email:email,
    }
    console.log("body",body)
    let res=await services.post(Url.FORGOT_PASSWORD,"",body)
    console.log("response login",res.isSuccess)
   
return res;
    }
}