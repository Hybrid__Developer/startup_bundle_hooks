
const colors = {
    kHexColor6368F2: '#6368F2',
    kHexColor45C1C0: '#45C1C0',
    kHexColor3073E3: '#3073E3',
    kHexColor24CBCC: '#24CBCC',
    kHexColorFF8A01: '#FF8A01',
    kHexColor35B9A8: '#35B9A8',
    themeColor: '#FF8A01',
    whiteColor: '#FFFFFF',
    fbTextColor: '#4574DB',
    googleColor: '#616161',
    blackColor: '#000000',
    grayColor: '#A0A0A0',
    tabBackgroundColor: '#F6F6F6',
    lightgrayColor: '#E1E1E1',
    blueColor: '#3166d1',
    lightBlueColor: '#c1d2f2',
    pinkColor: '#FF658E',
    yellowColor: '#FFAE01',
    darkGrayColor: '#565656',
    gameDetailColor: '#10b9b6',
    grayBackgroundColor: '#F2F2F2',
    lightYellowColor: '#fff7ee',
    graySeparater: '#e1e1e1',
    disableColor:"#cccccc"
  };
export default  colors;
