import React, { Component } from 'react'
// import Spinner from 'react-native-loading-spinner-overlay';
import AnimatedLoader from "react-native-animated-loader";
import { StyleSheet } from 'react-native';
class Loader extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
           
            <AnimatedLoader
            visible={this.props.isLoading}
            overlayColor="rgba(255,255,255,0.75)"
            source={require("../Assets/animationloader.json")}
            animationStyle={styles.lottie}
            speed={1}
          />
        )
    }

}

export default Loader
const styles = StyleSheet.create({
    lottie: {
        width: 100,
        height: 100
      }
});