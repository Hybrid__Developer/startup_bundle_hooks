import React, {useEffect, useState} from 'react';
import { Button, View,Text, Alert,StyleSheet,Image,TextInput,TouchableOpacity } from 'react-native';
import PATH from "./ImagePath"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../utility/index";

export default function SocialLoginBtn({onChange,style,Img}) {

  return (
      <View >
        <TouchableOpacity>
        <View 
        style={{height:50,width:wp("15%",),
        elevation: 10,
        shadowColor: '#199bf1',
        shadowOpacity: 0.4,
        shadowRadius: 5,
        shadowOffset: {
         width: 0, height: 2
        },
        backgroundColor:"white",
        alignItems:"center",
        justifyContent:"center",
        borderRadius:10,marginBottom:hp("3%")}}>
            <Image source={Img} style={style}></Image>
        </View>
        </TouchableOpacity>
   </View>
  );
}

const styles = StyleSheet.create({
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: "3%",
      },
      ImageStyle: {
        padding: 5,
        color: '#fff',
      },
      textInputView: {
        flex: 1,
        borderBottomColor: "black",
        borderBottomWidth: 1,
        marginRight: "10%"
    
      },
     
      txtInput: {
        fontSize: 20,
        color: "black",
      },
})
