const String = {
  logoTxt:"StartUp Bundle",
  loginTxt:"Login to your account",
  singIn:"Sign In",
  signInWith:"-----------Or Sign in with------------",
  signUpWith:"-----------Or Sign up with------------",
createAccount:"Create your Account",
  dontAccount:"Don't have an account?",
  signUpHere:" Sign up here",
  signUp:"Sign Up",
  forgotPassword:"Forgot your password",
  Next:"Next"
  };
  export default String;
