import React, {useEffect, useState} from 'react';
import { Button, View,Text, Alert,StyleSheet,Image,TextInput } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { NavigationActions, StackActions } from 'react-navigation';
import PATH from "./ImagePath"
import {useFormikContext} from 'formik';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../utility/index";

export default function TextField({onChange,placeHolder,userImg,style,security}) {

  return (
      <View >
         <View style={{marginTop:hp("3%"),alignSelf:"center",flexDirection:'row',marginLeft:wp("8%")}}>

<View style={{width:wp("70%"),backgroundColor:"white",justifyContent:"center",
 elevation: 10,
 shadowColor: '#199bf1',
 shadowOpacity: 0.2,
 shadowRadius: 5,
 shadowOffset: {
   width: 0, height: 2
 },
borderRadius:30,height:hp("4%"),}}>
<TextInput style={{marginLeft:wp("15%"),justifyContent:"center"}} 
secureTextEntry={security}
placeholder={placeHolder} placeholderTextColor="#199bf1" onChangeText={onChange}></TextInput>
</View>
<View style={{height:hp("6%"),width:wp("15%"),
alignItems:"center",justifyContent:"center",
elevation: 10,
shadowColor: '#199bf1',
shadowOpacity: 0.4,
shadowRadius: 5,
shadowOffset: {
 width: 0, height: 2
},
backgroundColor:"white",borderRadius:50,right:wp("75%"),bottom:10}}>
<Image source={userImg} style={style}></Image>
</View>
</View>

   </View>
  );
}

const styles = StyleSheet.create({
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: "3%",
      },
      ImageStyle: {
        padding: 5,
        color: '#fff',
      },
      textInputView: {
        flex: 1,
        borderBottomColor: "black",
        borderBottomWidth: 1,
        marginRight: "10%"
    
      },
     
      txtInput: {
        fontSize: 20,
        color: "black",
      },
})
