import {LOGIN_API } from "./Types";

const initialState = {
    user:{}
};
const  Reducers=(state= initialState, {type,payload})=>{
    switch(type){
        case  LOGIN_API:{
         return{
             ...state,
             user:payload
         }
        }
    }
}
export default Reducers;