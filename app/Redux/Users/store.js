// import { createStore,applyMiddleware } from "redux";
// import { composeWithDevTools } from "redux-devtools-extension";
// import { thunkMiddleware } from "redux-thunk";
// import Reducers from "./Reducers";
// import createLogger from "redux-logger"
// const logger = createLogger();
// const store = createStore(Reducers,
//     applyMiddleware(
//         thunkMiddleware, logger
//     )
// )
// const createStoreWithMiddleware = applyMiddleware(reduxPromise)(createStore);
// export default store




// import { createStore, combineReducers ,applyMiddleware} from 'redux';
// import reducers from "./Reducers";
// import { thunk } from "redux-thunk";
// const store = createStore(reducers, applyMiddleware(thunk));

// export default store;


import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './Reducers';

const store = createStore(
    reducers,
  applyMiddleware(thunk)
);
export default store;