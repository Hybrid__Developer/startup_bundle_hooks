import React from 'react';

import 'react-native-gesture-handler';

import {createStackNavigator} from '@react-navigation/stack';

import userProfile from "../screens/Account/userProfile"

const Stack = createStackNavigator();

const AccountNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
      
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen
        name="AccountScreen"
        component={userProfile}
        options={{headerShown: false}}
      />
     
    </Stack.Navigator>
  );
};

export default AccountNavigator;
