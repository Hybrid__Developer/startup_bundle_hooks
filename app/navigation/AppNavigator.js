import React from 'react';
import {Alert, Image, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import NewsFeedNavigator from "./NewsFeedNavigator"
import AccountNavigator from "./AccountNavigator"
// import AccountNavigator from "./AccountNavigator"
import PATH from '../Constants/ImagePath';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Tab = createBottomTabNavigator();

getTabBarVisibility = (route) => {
  const routeName = route.state
    ? route.state.routes[route.state.index].name
    : '';
console.log("srouteeeee///",route)
  if (
    routeName === 'userProfile' ||
    routeName === 'GameDetailRecord' ||
    routeName === 'GameRecordList' ||
    routeName === 'NewsFeedVideoPlayer' ||
    routeName === 'RegisterPlayer' ||
    routeName === 'RegisterReferee' ||
    routeName === 'CreateTeamForm1' ||
    routeName === 'CreateTeamForm2' ||
    routeName === 'CreateTeamForm3' ||
    routeName === 'CreateTeamForm4' ||
    routeName === 'CreateClubForm1' ||
    routeName === 'WritePostScreen' ||
    routeName === 'CreateClubForm2'||
    routeName=="WriteComment"
  ) {
    return false;
  }

  return true;
};

function AppNavigator() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: "orange",
        inactiveTintColor: "grey",
        labelStyle: {
          fontSize: 11,
          marginTop: -6,
          paddingTop: 5,
        },
        style: {
          tabBackgroundColor: "red",

          borderTopColor:"pink",
        },
      }}>
      <Tab.Screen
        name="Newsfeed"
        component={NewsFeedNavigator}
        
        options={({route}) => ({
          tabBarVisible: this.getTabBarVisibility(route),
          
          tabBarIcon: ({focused}) => (
            <TouchableOpacity onPress={()=>this.onAlertPress}>
            <Image
              source={PATH.password}
              style={focused ? styles.selectedTabImg : styles.tabImg}
            />
            </TouchableOpacity>
          ),
        })}
      />
      <Tab.Screen
        name="Search"
        component={AccountNavigator}
        options={{
          tabBarIcon: ({focused}) => (
            <Image
              source={PATH.password}
              style={focused ? styles.selectedTabImg : styles.tabImg}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Home"
        component={NewsFeedNavigator}
        
        options={{
          tabBarIcon: ({focused}) => (
            <Image
              source={PATH.logo}
              style={focused ? styles.selectedTabImg : styles.tabImg}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Notification"
        component={NewsFeedNavigator}
        options={{
          tabBarIcon: ({focused}) => (
            <Image
              source={PATH.password}
              style={focused ? styles.selectedTabImg : styles.tabImg}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Account"
        component={AccountNavigator}
        options={({route}) => ({
          tabBarVisible: this.getTabBarVisibility(route),
          tabBarIcon: ({focused}) => (
            <Image
              source={PATH.logo}
              style={focused ? styles.selectedTabImg : styles.tabImg}
            />
          ),
        })}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  tabImg: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    tintColor: "grey",
  },
  selectedTabImg: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    tintColor: "orange",
  },
});
export default AppNavigator;
