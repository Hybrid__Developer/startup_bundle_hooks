import React from 'react';
import 'react-native-gesture-handler';

import {createStackNavigator} from '@react-navigation/stack';

import WelcomeScreen from '../screens/AuthScreens/Welcome';
import SplashScreen from "../screens/AuthScreens/Splash"
import LoginScreen from "../screens/AuthScreens/Login"
import HomeScreen from "../screens/TabScreens/HomeScreen"
import SignUp from "../screens/AuthScreens/SignUp"
import ForgotPassword from "../screens/AuthScreens/ForgotPassword"

const Stack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: "white",
        headerTransparent: true,
        headerTitle: false,
        headerBackTitleVisible: false,
      }}>
     
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="WelcomeScreen"
        component={WelcomeScreen}
        options={{headerShown: false}}
      />
     <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{headerShown: false}}
      />
         <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default AuthNavigator;
