import React from 'react';

import 'react-native-gesture-handler';

import {createStackNavigator} from '@react-navigation/stack';

import NewsFeedList from "../screens/NewsFeed/NewsFeedList"
const Stack = createStackNavigator();

const NewsFeedNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        // headerTintColor: colors.blackColor,
        // headerTransparent: true,
        // headerTitle: true,
        headerBackTitleVisible: false,
      }}>

<Stack.Screen
        name="NewsFeedList"
        component={NewsFeedList}
        options={{headerShown: false}}
       
      />
      {/* <Stack.Screen
        name="FeedsScreen"
        component={FeedsScreen}
        options={{
          title: 'Newsfeed',
          headerTintColor: colors.blackColor,
          headerTitleStyle: {
            fontWeight: '500',
          },
          headerStyle: {
            backgroundColor: colors.whiteColor,
            borderBottomColor: colors.grayColor,
            borderBottomWidth: 0.3,
          },
        }}
      /> */}
       
        
    </Stack.Navigator>
  );
};

export default NewsFeedNavigator;
