import React, { useState,useEffect} from 'react';
import {  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert
  } from 'react-native';
import PATH from "../../../Constants/ImagePath"
import styles from "./style"
import * as api from "../../../Api/AuthApi"
import Loader  from "../../../Constants/Loader"
import TextField from "../../../Constants/TextField"
import String from "../../../Constants/String"
import SocialLoginBtn from "../../../Constants/SocialLoginBtn"
import * as Animatable from 'react-native-animatable';
function ForgotPassword({navigation}) {
  
  const [email, setEmail] = useState("sahilaery123@gmail.com")
  const [loader, setLoader] = useState(false)
  forgotPassword=async()=>{
    setLoader(true)
    let res =await api.ForgotPasswordApi(email)
    console.log("signUp screen",res)
    this.goToGmail()

    setLoader(false)
  }
  goToGmail = () => {
    Alert.alert(
        "",
        "Please check your Gmail",
        [
          
            { text: "Ohk", onPress: () => navigation.navigate("LoginScreen")},
        ],
    );
    return true;

}
  return (
   <View style={{backgroundColor:"#f2f2f2",height:"100%"}}>
    <ScrollView>
    <Loader isLoading={loader} />
    <TouchableOpacity onPress={()=>navigation.navigate("LoginScreen")}>
    <View style={styles.backView}>
      <Image source={PATH.back} style={styles.backImg}></Image>
    </View></TouchableOpacity>
    <Text style={styles.logoTxt}>{String.logoTxt}</Text>
  <Text style={styles.loginTxt}>{String.forgotPassword}</Text>
  <TextField userImg={PATH.email} style={styles.emailImg} placeHolder={"Enter your email"} onChange={email => setEmail(email)}></TextField>
  <TouchableOpacity onPress={this.forgotPassword}>
  <Animatable.View animation="fadeInUpBig" iterationCount={1} direction="alternate">
<View style={styles.loginBtn}>
  <Text style={styles.loginBtnTxt}>{String.Next}</Text>
</View>
</Animatable.View>
</TouchableOpacity>
   </ScrollView>
       </View>
  );
}
export default ForgotPassword;





















