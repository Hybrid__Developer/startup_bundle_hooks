import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../utility/index";
const styles = StyleSheet.create({

    backView: {
        marginLeft: wp("5%"),
        marginTop: hp("8%")
    },
    backImg: {
        height: 30, width: 30
    },

    loginTxt:
        { fontSize: 20, textAlign: "center", marginTop: "2%", marginBottom: "15%" },
    logoTxt:
        { fontSize: 35, textAlign: "center", marginTop: hp("5%"), },
    emailImg: {
        height: hp("2%"), width: wp("6.5%")
    },
    loginBtn: { height: 50, width: "70%", borderRadius: 30, backgroundColor: "#199bf1", justifyContent: "center", alignItems: "center", alignSelf: "center", marginTop: hp("20%") },
    loginBtnTxt: { fontSize: 20, color: "white",fontWeight:"bold" },
});



export default styles;