import React, { useState,useEffect} from 'react';
import {  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  } from 'react-native';
import PATH from "../../../Constants/ImagePath"
import styles from "./styles"
import * as api from "../../../Api/AuthApi"
import Loader  from "../../../Constants/Loader"
import TextField from "../../../Constants/TextField"
import String from "../../../Constants/String"
import SocialLoginBtn from "../../../Constants/SocialLoginBtn"
import * as Animatable from 'react-native-animatable';
import { useDispatch,useSelector } from "react-redux";
import { LoginApi } from "../../../Redux/Users/Actions";
import Reducers from '../../../Redux/Users/Reducers';
function LoginScreen({navigation}) {
  const [email, setEmail] = useState("")
  const [password, setPass] = useState("")
  const [loader, setLoader] = useState(false);

  const dispatch =  useDispatch();

  // const user = useselector(state=>Reducers.user);


  
//   login=async()=>{
//     setLoader(true)
// let res =await api.LoginApi(email,password)
//  setLoader(false)
//  if(res.isSuccess==true){
//  return navigation.navigate("HomeScreen")
//  }
//   }








  return (
   <View style={{backgroundColor:"#f2f2f2",height:"100%"}}>
    <ScrollView>
    <Loader isLoading={loader} />
   
      <View style={styles.logoView}>
        <Image source={PATH.logo} style={styles.logoImg}></Image>
      </View>
  <Text style={styles.logoTxt}>{String.logoTxt}</Text>
  <Text style={styles.loginTxt}>{String.loginTxt}</Text>
       <Animatable.View animation="fadeInDownBig" iterationCount={1} direction="alternate">
      <TextField userImg={PATH.email} style={styles.emailImg} placeHolder={"Email"} onChange={email => setEmail(email)}></TextField></Animatable.View>
      <Animatable.View animation="fadeInUpBig" iterationCount={1} direction="alternate">
      <TextField userImg={PATH.password} style={styles.textInputImg} security={true} placeHolder={"Password"} onChange={password => setPass(password)}></TextField>
      </Animatable.View>
      <Text style={styles.forgotTxt}onPress={()=>navigation.navigate("ForgotPassword")}>Forgot Password</Text>
      <TouchableOpacity onPress={()=>dispatch(LoginApi(email,password))}>
<View style={styles.loginBtn}>
  <Text style={styles.loginBtnTxt}>{String.singIn}</Text>
</View>
</TouchableOpacity>
  <Text style={styles.signInWith}>{String.signInWith}</Text>
  <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
  <SocialLoginBtn Img={PATH.google} style={styles.goggleImg}></SocialLoginBtn>
  <SocialLoginBtn Img={PATH.facebook} style={styles.facebookImg}></SocialLoginBtn>
  <SocialLoginBtn Img={PATH.twiter} style={styles.twitterImg}></SocialLoginBtn>
  </View>
  <View style={{flexDirection:"row",justifyContent:"center",marginBottom:"5%"}}>
  <Text style={{fontSize:20,color:"black"}}>{String.dontAccount}</Text>
  <Text style={{fontSize:20,color:"#199bf1"}} onPress={()=>navigation.navigate("SignUp")}>{String.signUp}</Text>
  </View>
   </ScrollView>
       </View>
  );
}
export default LoginScreen;





















