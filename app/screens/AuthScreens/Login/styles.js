import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../utility/index";
const styles = StyleSheet.create({

  logoView: {
   marginTop:hp("5%"),
   alignSelf:"center",justifyContent:"center"
  },
  logoView2: {
    alignSelf:"center",justifyContent:"center"
   },
  logoImg:
  {
   height:hp("20%"),
   width:wp("30%")
  },

  loginTxt:
  
    {fontSize:20,textAlign:"center",marginTop:"2%",marginBottom:"8%"}
  ,

  logoTxt:
  {fontSize:35,textAlign:"center"},
  loginBtn:{height:50,width:"50%",borderRadius:30,backgroundColor:"#199bf1",justifyContent:"center",alignItems:"center",alignSelf:"center",marginTop:"8%"},
  loginBtnTxt:{fontSize:20,color:"white"},
  signInWith:{fontSize:20,marginTop:"8%",marginBottom:"8%",textAlign:"center"},
  facebookImg:{height:hp("3.5%"),width:15},
  goggleImg:{
    height:hp("3%"),
    width:25
  },
  twitterImg:{
    height:hp("2.5%"),
    width:30
  },
  backView:{
    marginLeft:wp("5%"),
    marginTop:hp("8%")
  },
  backImg:{
    height:30,width:30
  },
  textInputImg:{height:hp("3%"),width:wp("5.5%")},
  emailImg:{
    height:hp("2%"),width:wp("6.5%")
  },
  forgotTxt:{textAlign:"center",color:"#199bf1",fontSize:20,marginLeft:wp("30%")}
});



export default styles;