import React, { useState,useEffect} from 'react';
import {  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert
  } from 'react-native';
import PATH from "../../../Constants/ImagePath"
import styles from "./style"
import * as api from "../../../Api/AuthApi"
import Loader  from "../../../Constants/Loader"
import TextField from "../../../Constants/TextField"
import String from "../../../Constants/String"
import SocialLoginBtn from "../../../Constants/SocialLoginBtn"
import * as Animatable from 'react-native-animatable';
function SignUp({navigation}) {
  const [FirstName, setFirstName] = useState("sahil")
  const [LastName, setLastName] = useState("sharma")
  const [email, setEmail] = useState("sahilaery123@gmail.com")
  const [password, setPass] = useState("123456789")
  const [loader, setLoader] = useState(false)
  signUp=async()=>{
    setLoader(true)
    let res =await api.SignUpApi(FirstName,LastName,email,password)
    console.log("signUp screen",res)
    setLoader(false)
 if(res.isSuccess==true){
    this.goToLogin()
 }
 else{
   Alert.alert("error",res.error)
 }
  }
  goToLogin = () => {
    Alert.alert(
        "",
        "User Register Sucessfull",
        [
          
            { text: "Ohk", onPress: () => navigation.navigate("LoginScreen")},

        ],
        { cancelable: false },
    );
    return true;

}
  return (
   <View style={{backgroundColor:"#f2f2f2",height:"100%"}}>
    <ScrollView>
    <Loader isLoading={loader} />

    <TouchableOpacity onPress={()=>navigation.navigate("LoginScreen")}>
    <View style={styles.backView}>
      <Image source={PATH.back} style={styles.backImg}></Image>
    </View></TouchableOpacity>
      <View style={styles.logoView2}>
        <Image source={PATH.logo} style={styles.logoImg}></Image>
      </View>
  <Text style={styles.logoTxt}>{String.logoTxt}</Text>
  <Text style={styles.loginTxt}>{String.createAccount}</Text>
  <Animatable.View animation="fadeInDownBig" iterationCount={1} direction="alternate">
      <TextField userImg={PATH.userBlue} style={styles.textInputImg} placeHolder={"FirstName"} onChange={FirstName => setFirstName(FirstName)}></TextField>
     </Animatable.View>
     <Animatable.View animation="fadeInDownBig" iterationCount={1} direction="alternate">
      <TextField userImg={PATH.userBlue} style={styles.textInputImg} placeHolder={"LastName"} onChange={LastName => setLastName(LastName)}></TextField>
     </Animatable.View>
  <Animatable.View animation="slideInLeft" iterationCount={1} direction="alternate">
      <TextField userImg={PATH.email} style={styles.emailImg} placeHolder={"Email"} onChange={email => setEmail(email)}></TextField>
      </Animatable.View>
  <Animatable.View animation="fadeInUpBig" iterationCount={1} direction="alternate">
      <TextField userImg={PATH.password} style={styles.textInputImg} security={true} placeHolder={"Password"} onChange={password => setPass(password)}></TextField>
      </Animatable.View>
      <TouchableOpacity onPress={this.signUp}>
<View style={styles.loginBtn}>
  <Text style={styles.loginBtnTxt}>{String.signUp}</Text>
</View>
</TouchableOpacity>
  <Text style={styles.signInWith}>{String.signUpWith}</Text>
  <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
  <SocialLoginBtn Img={PATH.google} style={styles.goggleImg}></SocialLoginBtn>
  <SocialLoginBtn Img={PATH.facebook} style={styles.facebookImg}></SocialLoginBtn>
  <SocialLoginBtn Img={PATH.twiter} style={styles.twitterImg}></SocialLoginBtn>
  </View>
   </ScrollView>
       </View>
  );
}
export default SignUp;





















