import React, {useEffect, useState} from 'react';
import {View, ImageBackground, Alert} from 'react-native';
import  PATH from '../../../Constants/ImagePath';
function SplashScreen({navigation}) {
 
  useEffect(() => {
    this.timeoutHandle = setTimeout(() => {
      this.retrieveData();
    }, 2000);
  });
  retrieveData = async () => {
   navigation.navigate("LoginScreen")
  };
  return (
   
   <View style={{width:"100%",height:"100%"}}>
     <ImageBackground source={PATH.splashLogin} style={{height:"100%",width:"100%"}}>

     </ImageBackground>
   </View>
  );
}

export default SplashScreen;
